# Dataverse - gestion des dataset

Quelques exemple de récupération/upload de données

## python
``` python
import requests
import json
from requests.structures import CaseInsensitiveDict

id = "doi:xx.xxxxx/xxxxx"
server = "https://serverdataverse.fr"
apikey = "xxxxxx-xxxxx-xxxx-xxxx-xxxxxxx"
url = server+"/api/datasets/:persistentId/?persistentId="+id

headers = CaseInsensitiveDict()
headers["X-Dataverse-key"] = apikey
r = requests.get(url, headers=headers)

r = json.loads(r.text)
# r = json.dumps(r)

print(r['data']['latestVersion']['files'])

```


## http
``` http
https://SERVER_URL/api/datasets/:persistentId/?key=APIKEY&persistentId=doi:DOI
```

## DVUploader
 Récupérer le .jar [ici](https://github.com/GlobalDataverseCommunityConsortium/dataverse-uploader/releases/)

``` bash
# uploader un fichier
java -jar DVUploader-v1.1.0.jar -key=APIKEY -did=doi:DOI -server=SERVER PATH

# uploader un répertoire
java -jar DVUploader-v1.1.0.jar -key=APIKEY -did=doi:DOI -server=SERVER PATH -recurse

```
